# About
A collection of scripts to handle mathematical terms.

## Deprecation Warning
This project will not be continued, as it was reimplemented in JavaScript
including client-side PDF
generation using texlive.js, LaTeX rendering in the browser using MathJax and a
much more useful internal format for terms. You should check
[here](https://gitlab.com/mknaf/mathterms-js) for the new project.

# Generation
Term generation is done in an "internal format" in `TermGen.py`.

# Parsing
Terms can be parsed and somewhat pretty-printed using `TermParser.py`.

# Multiplication
Terms can be multiplied using `TermMultiplier.py`.

# Examples
See the code in `main.py` for example usage. This will output
to STDOUT, so

    python main.py > file.html

should work if you want to write to a file, which makes sense
right now as the only pretty-printed format is html.
