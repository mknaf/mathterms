#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

class TermParser():

    """Parses a given mathematical term."""

    def __init__(self):
        # a regular expression on how a term may look like
        # when coming directly from TermGen.get_term()
        self._term_memb_re = re.compile("([+|-])([0-9]+)([a-z]+)")

    def parse(self, term):
        """Returns a list of triples containing matching term
        members. This will drop everything it cannot match.
        """
        #print(self._term_memb_re.findall(term))
        return self._term_memb_re.findall(term)

    def pretty(self, term, format_=None):
        """Pretty-print a term.
        """
        # a place to store our resulting string
        r = ""

        # if no format was given, output html
        if format_ is None:

            # for all the terms in the parse
            for memb in self.parse(term):
                r = r + " {} ".format(memb[0])  # sign
                r = r + memb[1]  # num

                # count the letters
                l = {}
                for a in memb[2]:
                    l[a] = l.setdefault(a, 0) + 1

                # for all the letters found
                for a in l.iteritems():
                    # do not write exponent if it is 1
                    if a[1] > 1:
                        r = r + "{}<sup>{}</sup>".format(a[0], a[1])
                    else:
                        r = r + a[0]

        return r

