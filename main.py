#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TermGen import TermGen
from TermMultiplier import TermMultiplier
from TermParser import TermParser


def main():
    tg = TermGen()
    tm = TermMultiplier()
    tp = TermParser()

    # get two terms
    t1 = tg.get_term()
    t2 = tg.get_term()

    # multiply the two terms
    print("({}) &times; ({}) = {}".format(
        tp.pretty(t1).strip("+ "),
        tp.pretty(t2).strip("+ "),
        tp.pretty(tm.multiply_terms(t1, t2).strip("+ "))
    ))


if __name__ == "__main__":
    main()
