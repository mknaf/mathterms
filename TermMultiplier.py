#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TermParser import TermParser


class TermMultiplier():

    """Multiply math terms."""

    def __init__(self):
        self.tp = TermParser()

    def multiply_terms(self, t1, t2):
        t1 = self.tp.parse(t1)
        t2 = self.tp.parse(t2)

        s = ""
        # for all the term members in term 1
        for m1 in t1:
            # for all the term members in term 2
            for m2 in t2:
                # multiply the two current terms
                s = s + self._multiply_membs(m1, m2)
        return s

    def _multiply_membs(self, m1, m2):
        # signedness
        if m1[0] == m2[0]:
            s = "+"
        else:
            s = "-"

        # numbers
        s = s + str(int(m1[1]) * int(m2[1]))

        # concatenate letters and sort
        alphas = list(m1[2] + m2[2])
        alphas.sort()

        # append sorted letters
        s = s + "".join(alphas)

        return s
