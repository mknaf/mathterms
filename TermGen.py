#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import string

class TermGen():

    """Generates random mathematical terms."""

    def __init__(self, max_num=10, max_alpha=2, max_exp=2, max_memb=2):
        """
        :max_num: the maximum value the numerical part can take
        :max_alpha: the maximum amount of alphabetical characters per term member
        :max_exp: the maximum value any exponent can take
        :max_memb: the maximum count of term members
        """

        self._max_num = max_num
        self._max_alpha = max_alpha
        self._max_exp = max_exp
        self._max_memb = max_memb

    def get_term(self, max_num=None, max_alpha=None, max_exp=None, max_memb=None):
        if max_num is None:
            max_num = self._max_num
        if max_alpha is None:
            max_alpha = self._max_alpha
        if max_exp is None:
            max_exp = self._max_exp
        if max_memb is None:
            max_memb = self._max_memb

        # prepare an empty string
        t = ""

        # fill the term with a random number of members
        for i in range(random.randint(2, max_memb)):
            t = t + self._get_term_member(max_num, max_alpha, max_exp)
        t = t.strip()

        return t

    def _get_term_member(self, max_num, max_alpha, max_exp):
        # start off with a sign
        m = random.choice(["+", "-"])

        # get a random number
        m = m + str(random.randint(1, max_num))

        # prepare a list of letters
        for i in range(max_alpha):
            a = random.choice(string.ascii_lowercase[:5]) * random.randint(1, max_exp)
            m = m + a

        return m

